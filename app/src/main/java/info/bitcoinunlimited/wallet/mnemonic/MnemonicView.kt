package info.bitcoinunlimited.wallet.mnemonic

import info.bitcoinunlimited.wallet.mnemonic.MnemonicViewIntent.* // ktlint-disable no-wildcard-imports
import info.bitcoinunlimited.wallet.utils.Event
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow

@ExperimentalCoroutinesApi
interface MnemonicView {
    /**
     * Intent to load the current Mnemonic state
     *
     * @return A flow that inits the current MnemonicViewState
     */
    fun initState(): Flow<Event<Boolean>>

    /**
     * Intent to submit the mnemonic
     *
     * @return A flow that emits the mnemonic
     */
    fun submitMnemonic(): Flow<Event<SubmitMnemonic?>>

    /**
     * Renders the MnemonicViewState
     *
     * @param state The current view state display
     */
    fun render(state: MnemonicViewState)
}
