package info.bitcoinunlimited.wallet.mnemonic

import info.bitcoinunlimited.wallet.room.Mnemonic

sealed class MnemonicViewState {
    data class MnemonicAvailable(
        val mnemonic: Mnemonic
    ) : MnemonicViewState()

    data class MnemonicErrorMessage(
        val message: String
    ) : MnemonicViewState()
}
