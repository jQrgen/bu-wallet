package info.bitcoinunlimited.wallet.mnemonic

import info.bitcoinunlimited.wallet.utils.Event
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow

@ExperimentalCoroutinesApi
class MnemonicViewIntent(
    val initState: MutableStateFlow<Event<Boolean>> = MutableStateFlow(Event(true)),
    val submitMnemonic: MutableStateFlow<Event<SubmitMnemonic?>> = MutableStateFlow(Event(null))
) {
    data class SubmitMnemonic(
        val phrase: String
    )
}
