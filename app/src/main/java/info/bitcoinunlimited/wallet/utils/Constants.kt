package info.bitcoinunlimited.wallet.utils

import bitcoinunlimited.libbitcoincash.ChainSelector

const val CONTENT_RECYCLER_VIEW_POSITION = "contentRecyclerViewPosition"

/**
 * Constants for VotePeer
 * @Firebase
 * Firebase-related paths
 * @Functions
 * Serverless node-js endpoints
 */
object Constants {
    const val region = "europe-west1"
    const val EndpointUrl: String = "https://$region-voter-6700b.cloudfunctions.net/"
    // TODO: Set when initiating library.
    val CURRENT_BLOCKCHAIN: ChainSelector = ChainSelector.BCHMAINNET
    const val REQUEST_CHALLENGE = "request_challenge"
    const val TWO_OPTION_ELECTION = "twooptionvote"
    const val RING_SIGNATURE_ELECTION = "ringsignature"
    const val SETUP_ACTION_BAR_WITH_NAV_CONTROLLER = "setupActionBarWithNavController"
    const val PRIVATE_KEY = "privateKey"
}
