package info.bitcoinunlimited.wallet.identity

import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.savedstate.SavedStateRegistryOwner
import bitcoinunlimited.libbitcoincash.PayAddress
import info.bitcoinunlimited.wallet.ElectrumAPI
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi

@DelicateCoroutinesApi
@ExperimentalUnsignedTypes
@ExperimentalCoroutinesApi
@InternalCoroutinesApi
class IdentityViewModelFactory(
    owner: SavedStateRegistryOwner,
    private val electrumApi: ElectrumAPI,
    private val address: PayAddress,
) : AbstractSavedStateViewModelFactory(owner, null) {
    override fun <T : ViewModel?> create(key: String, modelClass: Class<T>, state: SavedStateHandle) =
        IdentityViewModel(electrumApi, address) as T
}
