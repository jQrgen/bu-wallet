package info.bitcoinunlimited.wallet.identity

import info.bitcoinunlimited.wallet.utils.Event
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow

@ExperimentalCoroutinesApi
class IdentityViewIntent(
    val initState: MutableStateFlow<Event<Boolean>> = MutableStateFlow(Event(true)),
)
