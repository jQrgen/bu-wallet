package info.bitcoinunlimited.wallet.identity

import info.bitcoinunlimited.wallet.utils.Event
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow

@ExperimentalCoroutinesApi
interface IdentityView {
    /**
     * Intent to load the current Mnemonic state
     *
     * @return A flow that inits the current MnemonicViewState
     */
    fun initState(): Flow<Event<Boolean>>

    /**
     * Renders the ElectionDetailViewState
     *
     * @param state The current view state display
     */
    fun render(state: IdentityViewState)
}
