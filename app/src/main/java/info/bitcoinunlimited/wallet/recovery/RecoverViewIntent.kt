package info.bitcoinunlimited.wallet.recovery

import android.content.Context
import info.bitcoinunlimited.wallet.room.Mnemonic
import info.bitcoinunlimited.wallet.utils.Event
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow

@ExperimentalCoroutinesApi
class RecoverViewIntent(
    val initState: MutableStateFlow<Event<Boolean>> = MutableStateFlow(Event(true)),
    val recoverMnemonic: MutableStateFlow<Event<RecoverMnemonic?>> = MutableStateFlow(Event(null))
) {
    data class RecoverMnemonic(
        val mnemonic: Mnemonic,
        val context: Context
    )
}
