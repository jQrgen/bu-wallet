package info.bitcoinunlimited.wallet

import info.bitcoinunlimited.wallet.room.Mnemonic

object MnemonicMock {
    fun getMnemonic(): Mnemonic {
        return Mnemonic("cupboard two beach document doctor toward behave attend cliff mesh dove stool")
    }
}
