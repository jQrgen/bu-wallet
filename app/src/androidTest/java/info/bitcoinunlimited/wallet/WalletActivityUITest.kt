package info.bitcoinunlimited.wallet

import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Lifecycle
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.ApplicationProvider.getApplicationContext
import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.* // ktlint-disable no-wildcard-imports
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.DrawerActions
import androidx.test.espresso.contrib.DrawerMatchers.isClosed
import androidx.test.espresso.matcher.ViewMatchers.* // ktlint-disable no-wildcard-imports
import info.bitcoinunlimited.wallet.room.Mnemonic
import info.bitcoinunlimited.wallet.utils.InjectorUtilsApp
import io.mockk.clearAllMocks
import io.mockk.mockk
import kotlinx.coroutines.* // ktlint-disable no-wildcard-imports
import kotlinx.coroutines.test.runTest
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.TypeSafeMatcher
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

@DelicateCoroutinesApi
@ExperimentalUnsignedTypes
@ExperimentalCoroutinesApi
@InternalCoroutinesApi
class WalletActivityUITest {
    private val intent = InjectorUtilsApp.getVotingActivityIntent(
        getApplicationContext(), VotePeerMock.mockPrivateKey(), mockk(relaxed = true), Mnemonic("mock")
    )

    private lateinit var scenario: ActivityScenario<WalletActivity>

    @BeforeEach
    fun setUp() = runTest {
        scenario = launchActivity(intent)
        scenario.moveToState(Lifecycle.State.RESUMED)
    }

    @AfterEach
    fun clear() = runTest {
        scenario.moveToState(Lifecycle.State.DESTROYED)
        clearAllMocks()
    }

    @Test
    fun open_menu_drawer_display_nav_items() = runTest {
        onView(withId(R.id.drawer_layout))
            .check(matches(isClosed(Gravity.LEFT)))
            .perform(DrawerActions.open())

        onView(withId(R.id.menu_nav_identity)).check(matches(isDisplayed()))
        onView(withId(R.id.menu_nav_mnemonic)).check(matches(isDisplayed()))
    }

    @Test fun open_menu_drawer_navigate_to_election_master() = runTest {
        onView(withId(R.id.drawer_layout))
            .perform(DrawerActions.open())
        onView(withId(R.id.drawer_layout))
            .perform(DrawerActions.open())
    }

    @Test
    fun navigate_to_identity_test() = runTest {
        onView(withId(R.id.drawer_layout))
            .perform(DrawerActions.open())
        onView(withId(R.id.menu_nav_identity))
            .perform(click())

        onView(withId(R.id.address_layout)).check(matches(isDisplayed()))
        onView(withId(R.id.identity_qr_code)).check(matches(isDisplayed()))
        onView(withId(R.id.text_current_balance)).check(matches(isDisplayed()))
        onView(withId(R.id.layout_share_identity)).check(matches(isDisplayed()))
        onView(withId(R.id.identity_share_image)).check(matches(isDisplayed()))
        onView(withId(R.id.identity_share_text)).check(matches(isDisplayed()))
    }

    @Test
    fun navigate_to_mnemonic() = runTest {
        onView(withId(R.id.drawer_layout))
            .perform(DrawerActions.open())
        onView(withId(R.id.menu_nav_mnemonic))
            .perform(click())

        onView(withId(R.id.mnemonic_headline)).check(matches(isDisplayed()))
        onView(withId(R.id.mnemonic_display)).check(matches(isDisplayed()))
        onView(withId(R.id.recover_mnemonic_button)).check(matches(isDisplayed()))
        onView(withId(R.id.mnemonic_error_message)).check(matches(isDisplayed()))
    }

    @Test
    fun navigate_to_recover() = runTest {
        onView(withId(R.id.drawer_layout))
            .perform(DrawerActions.open())
        onView(withId(R.id.menu_nav_mnemonic))
            .perform(click())
        onView(withId(R.id.recover_mnemonic_button))
            .perform(click())
    }

    private fun childAtPosition(
        parentMatcher: Matcher<View>,
        position: Int
    ): Matcher<View> {

        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("Child at position $position in parent ")
                parentMatcher.describeTo(description)
            }

            public override fun matchesSafely(view: View): Boolean {
                val parent = view.parent
                return parent is ViewGroup && parentMatcher.matches(parent) &&
                    view == parent.getChildAt(position)
            }
        }
    }

    @Test fun same_destination_clicked() {
        // TODO: Implement. See VotingActivity.sameDestinationClicked()
    }

    @Test
    fun is_offline() {
        // TODO: Implement with VotePeerActivityViewState.ConnectionStatus
    }

    @Test
    fun is_online() {
        // TODO: Implement with VotePeerActivityViewState.ConenctionStatus
    }

    @Test
    fun displays_error() {
        // TODO: Implement with VotePeerActivityState.VotePeerActivityError
    }
}
